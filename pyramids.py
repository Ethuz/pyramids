class Pyramid:


    def __init__(self, line):
        self.symb = line
        self.pyramids = self.to_pyramids()


    def to_pyramids(self):
        '''
        Возвращает список четырехсимвольных пирамид, 
        из которых состоит основная пирамида, упорядоченных начиная с вершины
        '''
        pyr_amount = int(len(self.symb) / 4)
        pyramids = []
        row = 1
        top = 0
        bottom = 4
        new_row = 4
        for _ in range(pyr_amount):
            pyr = self.symb[top] + self.symb[bottom - 3: bottom]
            pyramids.append(pyr)
            if bottom == new_row:
                row += 1
                top, bottom = bottom, bottom + 4 * row
                new_row = 4 * row ** 2
                continue
            top, bottom = bottom, top + 4   
        return pyramids


    def compress(self):
        '''
        Сжимает четырехсимвольные пирамиды в списке пирамид, 
        если все символы каждой из пирамид равны.
        Изменяет символьное представление основной пирамиды 
        на основании сжатого списка пирамид.
        Возвращает True - если сжатие удалось, 
        False - если невозможно сжать основную пирамиду
        '''
        if self.pyramids == []:
            return False
        new_pyramids = []
        for pyramid in self.pyramids:
            if len(set(pyramid)) == 1:
                new_pyramids.append(pyramid[0])
            else:
                return False
        self.symb = ''.join(new_pyramids)
        self.pyramids = self.to_pyramids()
        return True


   
line = input()[::-1]
pyramid = Pyramid(line)
while pyramid.compress():
    pass
print(pyramid.symb[::-1])